# Shared

A repository that contains shared functionalities for assignment repositories, like visualization, models, etcetera.

## How to use the shared functionality repository in your own repository?##
Assignment repositories can include this repository with shared functionality by specifying this repository as a sub-module. To this end, the following steps need to be performed:

### Step 1: Import the shared repository as a sub-module ###
Run the following command in the root folder of the repository in which you want to use the shared repository:
```
git submodule add https://gitlab.tudelft.nl/active-mode-mooc/shared
```
As a result, a  *.gitmodules* file is created and added to the assignments root folder. A *shared* folder is also created as a sub-folder of the assignment repositories root folder, with its own *.git* file and repository content.

### Step 2: Commit generated files ###
Either use a GUI like SourceTree, or commit the changes from the terminal:
```
git add -all
git commit -m "Imported shared repository as sub-module"
```

From now on, files from the *shared* repository can be used by files from the assignment repository. Modifications to the *shared* folder are maintained separately and have to be committed on the *shared* repository. Modifications outside the *shared* folder are maintained and committed through the assignment repository. 


